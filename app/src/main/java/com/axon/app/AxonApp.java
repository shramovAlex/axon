package com.axon.app;

import android.app.Application;

import com.axon.di.component.AppComponent;
import com.axon.di.component.DaggerAppComponent;
import com.axon.di.module.AppModule;

/**
 * @author Alexandr Shramov
 * @date 10.12.19
 */
public class AxonApp extends Application {

    protected static AxonApp sApp;
    protected AppComponent mAppComponent;

    public static AxonApp getApplication() {
        return sApp;
    }

    public AppComponent getAppComponent() {
        return mAppComponent;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        sApp = this;
        initAppComponent();
    }

    private void initAppComponent() {
        mAppComponent = DaggerAppComponent
                .builder().appModule(new AppModule(this))
                .build();
    }

}
