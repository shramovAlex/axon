package com.axon.mvp.view;

import com.axon.domain.entity.Name;
import com.axon.mvp.view.common.BaseFragmentView;

import moxy.viewstate.strategy.AddToEndSingleStrategy;
import moxy.viewstate.strategy.StateStrategyType;

/**
 * @author Alexandr Shramov
 * @date 11.12.19
 */
public interface UserFragmentView extends BaseFragmentView {

    @StateStrategyType(AddToEndSingleStrategy.class)
    void initView();

    @StateStrategyType(AddToEndSingleStrategy.class)
    void loadUserAvatar(String url);

    @StateStrategyType(AddToEndSingleStrategy.class)
    void setUserName(Name userName);

    @StateStrategyType(AddToEndSingleStrategy.class)
    void setUserAge(int age);

    @StateStrategyType(AddToEndSingleStrategy.class)
    void setUserDateOfBirth(String dateOfBirth);

    @StateStrategyType(AddToEndSingleStrategy.class)
    void setUserGender(String gender);

    @StateStrategyType(AddToEndSingleStrategy.class)
    void setUserPhone(String phone);

    @StateStrategyType(AddToEndSingleStrategy.class)
    void setUserEmail(String email);

}
