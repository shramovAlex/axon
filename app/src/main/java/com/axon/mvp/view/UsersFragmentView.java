package com.axon.mvp.view;

import com.axon.domain.entity.User;
import com.axon.mvp.view.common.BaseFragmentView;

import java.util.ArrayList;

import moxy.viewstate.strategy.AddToEndSingleStrategy;
import moxy.viewstate.strategy.AddToEndStrategy;
import moxy.viewstate.strategy.OneExecutionStateStrategy;
import moxy.viewstate.strategy.StateStrategyType;

/**
 * @author Alexandr Shramov
 * @date 10.12.19
 */
public interface UsersFragmentView extends BaseFragmentView {

    @StateStrategyType(AddToEndSingleStrategy.class)
    void initUsersRv();

    @StateStrategyType(AddToEndSingleStrategy.class)
    void showProgressBar();

    @StateStrategyType(AddToEndStrategy.class)
    void setUsers(ArrayList<User> users);

    @StateStrategyType(AddToEndSingleStrategy.class)
    void showError(String message);

    @StateStrategyType(OneExecutionStateStrategy.class)
    void transferToUserFragment(User user);

}
