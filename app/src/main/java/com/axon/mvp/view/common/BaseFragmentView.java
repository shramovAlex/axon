package com.axon.mvp.view.common;

import moxy.MvpView;

/**
 * @author Alexandr Shramov
 * @date 10.12.19
 */
public interface BaseFragmentView extends MvpView {
}
