package com.axon.mvp.presenter;

import com.axon.app.AxonApp;
import com.axon.domain.entity.User;
import com.axon.mvp.presenter.common.BaseFragmentPresenter;
import com.axon.mvp.view.UserFragmentView;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import moxy.InjectViewState;

import static com.axon.utils.Constants.API_DATE_FORMAT;
import static com.axon.utils.Constants.APP_DATE_FORMAT;

/**
 * @author Alexandr Shramov
 * @date 11.12.19
 */
@InjectViewState
public class UserFragmentPresenter extends BaseFragmentPresenter<UserFragmentView> {

    private User mUser;

    public UserFragmentPresenter(User user) {
        mUser = user;
    }

    @Override
    protected void inject() {
        AxonApp.getApplication().getAppComponent().inject(this);
    }

    @Override
    protected void onFirstViewAttach() {
        super.onFirstViewAttach();
        getViewState().initView();
        if(mUser.getPicture() != null) {
            getViewState().loadUserAvatar(mUser.getPicture().getLargeUrl());
        }
        getViewState().setUserName(mUser.getName());
        if(mUser.getDateOfBirth() != null) {
            getViewState().setUserAge(mUser.getDateOfBirth().getAge());
            Date date = null;
            try {
                date = new SimpleDateFormat(API_DATE_FORMAT, Locale.getDefault()).parse(mUser.getDateOfBirth().getDate());
            } catch (Exception e) {
                e.printStackTrace();
            }
            if(date != null) {
                final String formattedDate = new SimpleDateFormat(APP_DATE_FORMAT, Locale.getDefault()).format(date);
                getViewState().setUserDateOfBirth(formattedDate);
            }
        }
        getViewState().setUserGender(mUser.getGender());
        getViewState().setUserEmail(mUser.getEmail());
        getViewState().setUserPhone(mUser.getCell());
    }
}
