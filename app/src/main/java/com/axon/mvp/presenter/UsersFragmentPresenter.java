package com.axon.mvp.presenter;

import android.util.Log;

import com.axon.app.AxonApp;
import com.axon.domain.entity.PaginatedDataWrapper;
import com.axon.domain.entity.User;
import com.axon.domain.params.GetUsersParam;
import com.axon.domain.usecase.GetUsersUseCase;
import com.axon.mvp.presenter.common.BaseFragmentPresenter;
import com.axon.mvp.view.UsersFragmentView;
import com.axon.ui.adapter.clickdata.UserClickData;
import com.axon.utils.Constants;

import java.util.ArrayList;

import javax.inject.Inject;

import io.reactivex.subjects.PublishSubject;
import moxy.InjectViewState;

import static com.axon.utils.Constants.LOG_DEFAULT_TAG;

/**
 * @author Alexandr Shramov
 * @date 10.12.19
 */
@InjectViewState
public class UsersFragmentPresenter extends BaseFragmentPresenter<UsersFragmentView> {

    @Inject GetUsersUseCase mGetUsersUseCase;

    private String mSeed;
    private int mCurrentPage = Constants.DEFAULT_PAGE_TO_LOAD;
    private boolean mIsLoading;

    @Override
    protected void inject() {
        AxonApp.getApplication().getAppComponent().inject(this);
    }

    @Override
    protected void release() {
        super.release();
        mGetUsersUseCase.unsubscribe();
    }

    @Override
    protected void onFirstViewAttach() {
        super.onFirstViewAttach();
        getViewState().initUsersRv();
        getUsers();
    }

    private void getUsers() {
        mIsLoading = true;
        getViewState().showProgressBar();

        mGetUsersUseCase.execute(new GetUsersParam(mCurrentPage, Constants.DEFAULT_ITEMS_TO_LOAD, mSeed),
                                 this::onSuccessGetUsers,
                                 this::onError);
    }

    private void onSuccessGetUsers(PaginatedDataWrapper<ArrayList<User>> wrapper) {
        mSeed = wrapper.getPaginationInfo().getSeed();
        getViewState().setUsers(wrapper.getData());
        mIsLoading = false;
    }

    private void onError(Throwable throwable) {
        Log.e(LOG_DEFAULT_TAG, throwable.getLocalizedMessage());
        throwable.printStackTrace();
        getViewState().showError(throwable.getLocalizedMessage());
        mIsLoading = false;
    }

    public void onLoadMore() {
        if(!mIsLoading) {
            mCurrentPage++;
            getUsers();
        }
    }

    public void observeUserClick(PublishSubject<UserClickData> subject) {
        mCompositeDisposable.add(
                subject.subscribe(
                        userClickData -> {
                            if(userClickData.getType().equals(UserClickData.USER_CLICK)) {
                                getViewState().transferToUserFragment(userClickData.getUser());
                            }
                        },
                        this::onError
                )
        );
    }
}
