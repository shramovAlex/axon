package com.axon.mvp.presenter.common;

import com.axon.mvp.view.common.BaseFragmentView;

import io.reactivex.disposables.CompositeDisposable;
import moxy.MvpPresenter;

/**
 * @author Alexandr Shramov
 * @date 10.12.19
 */
public abstract class BaseFragmentPresenter<ViewType extends BaseFragmentView> extends MvpPresenter<ViewType> {

    protected CompositeDisposable mCompositeDisposable = new CompositeDisposable();

    public BaseFragmentPresenter() {
        inject();
    }

    protected abstract void inject();

    @Override
    public void onDestroy() {
        super.onDestroy();
        release();
    }

    protected void release() {
        mCompositeDisposable.dispose();
        // for further implementation
    }

}
