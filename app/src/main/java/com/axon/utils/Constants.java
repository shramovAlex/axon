package com.axon.utils;

/**
 * @author Alexandr Shramov
 * @date 10.12.19
 */
public final class Constants {

    private Constants() {}

    public final static String BASE_URL = "https://randomuser.me/";
    public final static String API_DATE_FORMAT = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'";
    public final static String APP_DATE_FORMAT = "yyyy-MM-dd";

    public final static int DEFAULT_PAGE_TO_LOAD = 1;
    public final static int DEFAULT_ITEMS_TO_LOAD = 20;

    public final static String LOG_DEFAULT_TAG = "axon";

}
