package com.axon.ui.activity;

import android.os.Bundle;

import com.axon.R;
import com.axon.app.AxonApp;
import com.axon.manager.common.Router;

import javax.inject.Inject;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;

public class MainActivity extends AppCompatActivity {

    @Inject Router mRouter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AxonApp.getApplication().getAppComponent().inject(this);
        setContentView(R.layout.activity_main);
        mRouter.transferToUsersFragment(this);
    }

    @Override
    public void onBackPressed() {
        final FragmentManager fragmentManager = getSupportFragmentManager();
        if(fragmentManager.getBackStackEntryCount() > 1) {
            fragmentManager.popBackStack();
        } else {
            super.onBackPressed();
        }
    }
}
