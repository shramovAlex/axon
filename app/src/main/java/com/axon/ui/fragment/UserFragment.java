package com.axon.ui.fragment;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.telephony.PhoneNumberUtils;
import android.util.Log;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.axon.R;
import com.axon.app.AxonApp;
import com.axon.domain.entity.Name;
import com.axon.domain.entity.User;
import com.axon.manager.ImageManager;
import com.axon.mvp.presenter.UserFragmentPresenter;
import com.axon.mvp.view.UserFragmentView;
import com.axon.ui.fragment.common.BaseFragment;

import java.util.Locale;

import javax.inject.Inject;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import butterknife.BindView;
import moxy.presenter.InjectPresenter;
import moxy.presenter.ProvidePresenter;

/**
 * @author Alexandr Shramov
 * @date 11.12.19
 */
public class UserFragment extends BaseFragment implements UserFragmentView {

    public static final String TAG = "UserFragment";
    private static final String ARG_USER = "ARG_USER";
    private static final int REQUEST_CALL_PHONE = 100;

    @InjectPresenter UserFragmentPresenter mPresenter;
    @Inject ImageManager mImageManager;

    @BindView(R.id.fragment_user_avatar) ImageView mUserAvatarImage;
    @BindView(R.id.fragment_user_name) TextView mUserNameTv;
    @BindView(R.id.fragment_user_age) TextView mUserAgeTv;
    @BindView(R.id.fragment_user_date_of_birth) EditText mDateOfBirthEt;
    @BindView(R.id.fragment_user_gender) EditText mUserGenderEt;
    @BindView(R.id.fragment_user_cell_phone) EditText mUserCellPhoneEt;
    @BindView(R.id.fragment_user_email) EditText mUserEmailEt;

    public static UserFragment newInstance(User user) {
        final UserFragment fragment = new UserFragment();
        final Bundle args = new Bundle();
        args.putParcelable(ARG_USER, user);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AxonApp.getApplication().getAppComponent().inject(this);
    }

    @ProvidePresenter
    UserFragmentPresenter providePresenter() {
        User user = null;
        if (getArguments() != null) {
            user = getArguments().getParcelable(ARG_USER);
        }
        return new UserFragmentPresenter(user);
    }

    @Override
    protected int getLayout() {
        return R.layout.fragment_user;
    }

    @Override
    public void initView() {
        mUserCellPhoneEt.setOnClickListener(view -> {
            if(getActivity() != null) {
                int checkPermission = ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.CALL_PHONE);
                if (checkPermission == PackageManager.PERMISSION_GRANTED) {
                    makeCall();
                } else {
                    ActivityCompat.requestPermissions(
                            getActivity(),
                            new String[]{Manifest.permission.CALL_PHONE},
                            REQUEST_CALL_PHONE);
                }
            }
        });
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (getActivity() != null && requestCode == REQUEST_CALL_PHONE) {
            if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.CALL_PHONE) == PackageManager.PERMISSION_GRANTED) {
                makeCall();
            } else {
                Log.i(TAG, "Contacts permissions were NOT granted.");
            }
        } else {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    @SuppressLint("MissingPermission")
    private void makeCall() {
        if(getActivity() != null) {
            final Intent intent = new Intent(Intent.ACTION_CALL);
            final String phoneNumber = mUserCellPhoneEt.getText().toString();
            String formattedPhoneNumber;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                formattedPhoneNumber = PhoneNumberUtils.formatNumber(phoneNumber, Locale.getDefault().getCountry());
            } else {
                formattedPhoneNumber = PhoneNumberUtils.formatNumber(phoneNumber);
            }
            intent.setData(Uri.parse("tel:" + formattedPhoneNumber));
            getActivity().startActivity(intent);
        }
    }

    @Override
    public void loadUserAvatar(String url) {
        mImageManager.loadImage(mUserAvatarImage, url);
    }

    @Override
    public void setUserName(Name userName) {
        mUserNameTv.setText(getString(R.string.user_name, userName.getFirst(), userName.getLast()));
    }

    @Override
    public void setUserAge(int age) {
        mUserAgeTv.setText(getString(R.string.user_date_of_birth, age));
    }

    @Override
    public void setUserGender(String gender) {
        mUserGenderEt.setText(gender);
    }

    @Override
    public void setUserDateOfBirth(String dateOfBirth) {
        mDateOfBirthEt.setText(dateOfBirth);
    }

    @Override
    public void setUserPhone(String phone) {
        mUserCellPhoneEt.setText(phone);
    }

    @Override
    public void setUserEmail(String email) {
        mUserEmailEt.setText(email);
    }
}
