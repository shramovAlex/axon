package com.axon.ui.fragment;

import android.os.Bundle;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.axon.R;
import com.axon.app.AxonApp;
import com.axon.domain.entity.User;
import com.axon.manager.ImageManager;
import com.axon.manager.common.Router;
import com.axon.mvp.presenter.UsersFragmentPresenter;
import com.axon.mvp.view.UsersFragmentView;
import com.axon.ui.adapter.UsersAdapter;
import com.axon.ui.fragment.common.BaseFragment;

import java.util.ArrayList;

import javax.inject.Inject;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import moxy.presenter.InjectPresenter;

/**
 * @author Alexandr Shramov
 * @date 10.12.19
 */
public class UsersFragment extends BaseFragment implements UsersFragmentView {

    public static final String TAG = "UsersFragment";

    @InjectPresenter UsersFragmentPresenter mPresenter;
    @Inject ImageManager mImageManager;
    @Inject Router mRouter;

    @BindView(R.id.fragment_users_progress_bar) ProgressBar mProgressBar;
    @BindView(R.id.fragment_users_rv) RecyclerView mUsersRv;
    @BindView(R.id.fragment_users_error_tv) TextView mErrorTv;

    private UsersAdapter mAdapter;

    @Override
    protected int getLayout() {
        return R.layout.fragment_users;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        AxonApp.getApplication().getAppComponent().inject(this);
    }

    @Override
    public void initUsersRv() {
        mAdapter = new UsersAdapter(mImageManager);
        mUsersRv.setLayoutManager(new LinearLayoutManager(mUsersRv.getContext(),
                                                          LinearLayoutManager.VERTICAL,
                                                          false));
        mUsersRv.setAdapter(mAdapter);
        mUsersRv.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy)
            {
                if (!recyclerView.canScrollVertically(1)) {
                    mPresenter.onLoadMore();
                }
            }
        });
        mPresenter.observeUserClick(mAdapter.observeItemClick());
    }

    @Override
    public void showProgressBar() {
        mProgressBar.setVisibility(View.VISIBLE);
        mErrorTv.setVisibility(View.GONE);
    }

    @Override
    public void setUsers(ArrayList<User> users) {
        mAdapter.insertData(users);
        mProgressBar.setVisibility(View.GONE);
        mUsersRv.setVisibility(View.VISIBLE);
        mErrorTv.setVisibility(View.GONE);
    }

    @Override
    public void showError(String message) {
        mErrorTv.setText(message);
        mProgressBar.setVisibility(View.GONE);
        mUsersRv.setVisibility(View.GONE);
        mErrorTv.setVisibility(View.VISIBLE);
    }

    @Override
    public void transferToUserFragment(User user) {
        mRouter.transferToUserFragment(getActivity(), user);
    }
}
