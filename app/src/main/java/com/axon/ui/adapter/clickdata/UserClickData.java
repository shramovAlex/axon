package com.axon.ui.adapter.clickdata;

import com.axon.domain.entity.User;

/**
 * @author Alexandr Shramov
 * @date 10.12.19
 */
public class UserClickData {

    public static final String USER_CLICK = "USER_CLICK";

    private String mType;
    private User mUser;

    public UserClickData(String type, User user) {
        mType = type;
        mUser = user;
    }

    public String getType() {
        return mType;
    }

    public User getUser() {
        return mUser;
    }
}
