package com.axon.ui.adapter.holder;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.axon.R;
import com.axon.domain.entity.Location;
import com.axon.domain.entity.Name;
import com.axon.domain.entity.User;
import com.axon.manager.ImageManager;
import com.axon.ui.adapter.clickdata.UserClickData;
import com.axon.ui.adapter.holder.common.BaseHolder;

import androidx.annotation.NonNull;
import butterknife.BindView;
import io.reactivex.subjects.PublishSubject;

/**
 * @author Alexandr Shramov
 * @date 10.12.19
 */
public class UserHolder extends BaseHolder<User, UserClickData> {

    @BindView(R.id.item_user_container) View mUserContainer;
    @BindView(R.id.item_user_avatar_image) ImageView mUserAvatar;
    @BindView(R.id.item_user_name) TextView mUserName;
    @BindView(R.id.item_user_email) TextView mEmailTv;
    @BindView(R.id.item_user_location) TextView mLocationTv;

    private final ImageManager mImageManager;

    public UserHolder(@NonNull View itemView,
                      PublishSubject<UserClickData> click,
                      ImageManager imageManager) {
        super(itemView, click);
        mImageManager = imageManager;
        mUserContainer.setOnClickListener(view -> {
            mClick.onNext(new UserClickData(UserClickData.USER_CLICK, mItem));
        });
    }

    @Override
    protected void bind() {
        loadAvatar(mItem.getPicture().getThumbnail());
        setUserName(mItem.getName());
        setEmail(mItem.getEmail());
        setLocation(mItem.getLocation());
    }

    private void loadAvatar(String url) {
        mImageManager.loadImage(mUserAvatar, url);
    }

    private void setUserName(Name name) {
        mUserName.setText(mUserName.getContext().getString(R.string.user_name, name.getFirst(), name.getLast()));
    }

    private void setEmail(String email) {
        mEmailTv.setText(email);
    }

    private void setLocation(Location location) {
        mLocationTv.setText(mLocationTv.getContext().getString(R.string.user_location,
                                                               location.getState(),
                                                               location.getCity(),
                                                               String.valueOf(location.getStreet().getNumber()),
                                                               location.getStreet().getName()));
    }
}
