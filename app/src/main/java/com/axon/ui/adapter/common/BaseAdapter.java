package com.axon.ui.adapter.common;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.axon.domain.entity.common.DisplayableItem;
import com.axon.ui.adapter.holder.common.BaseHolder;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import io.reactivex.subjects.PublishSubject;

/**
 * @author Alexandr Shramov
 * @date 10.12.19
 */
public abstract class BaseAdapter
        <DataType extends DisplayableItem, HolderType extends BaseHolder<DataType, ClickType>, ClickType>
        extends RecyclerView.Adapter<HolderType> {

    private int mLayoutId;
    private ArrayList<DataType> mData = new ArrayList<>();
    private PublishSubject<ClickType> mClick = PublishSubject.create();

    public BaseAdapter(int layoutId) {
        mLayoutId = layoutId;
    }

    @NonNull
    @Override
    public HolderType onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        final View view = LayoutInflater.from(parent.getContext()).inflate(mLayoutId, parent, false);
        return createViewHolder(view, viewType, mClick);
    }

    protected abstract HolderType createViewHolder(View view, int viewType, PublishSubject<ClickType> click);

    @Override
    public void onBindViewHolder(@NonNull HolderType holder, int position) {
        holder.onBind(mData.get(position));
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    public ArrayList<DataType> getData() {
        return mData;
    }

    public void setData(ArrayList<DataType> data) {
        mData.clear();
        mData.addAll(data);
        notifyDataSetChanged();
    }

    public void insertData(ArrayList<DataType> data) {
        final int position = getItemCount();
        mData.addAll(data);
        notifyItemRangeInserted(position, data.size());
    }

    public PublishSubject<ClickType> observeItemClick() {
        return mClick;
    }
}
