package com.axon.ui.adapter.holder.common;

import android.view.View;

import com.axon.domain.entity.common.DisplayableItem;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import io.reactivex.subjects.PublishSubject;

/**
 * @author Alexandr Shramov
 * @date 10.12.19
 */
public abstract class BaseHolder<DataType extends DisplayableItem, ClickType> extends RecyclerView.ViewHolder {

    protected DataType mItem;
    protected PublishSubject<ClickType> mClick;
    private Unbinder mUnbinder;

    public BaseHolder(@NonNull View itemView, PublishSubject<ClickType> click) {
        super(itemView);
        mClick = click;
        mUnbinder = ButterKnife.bind(this, itemView);
    }

    public final void onBind(DataType item) {
        mItem = item;
        bind();
    }

    protected abstract void bind();
}
