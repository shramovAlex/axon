package com.axon.ui.adapter;

import android.view.View;

import com.axon.R;
import com.axon.domain.entity.User;
import com.axon.manager.ImageManager;
import com.axon.ui.adapter.clickdata.UserClickData;
import com.axon.ui.adapter.common.BaseAdapter;
import com.axon.ui.adapter.holder.UserHolder;

import io.reactivex.subjects.PublishSubject;

/**
 * @author Alexandr Shramov
 * @date 10.12.19
 */
public class UsersAdapter extends BaseAdapter<User, UserHolder, UserClickData> {

    private ImageManager mImageManager;

    public UsersAdapter(ImageManager imageManager) {
        super(R.layout.item_user);
        mImageManager = imageManager;
    }

    @Override
    protected UserHolder createViewHolder(View view, int viewType, PublishSubject<UserClickData> click) {
        return new UserHolder(view, click, mImageManager);
    }
}
