package com.axon.manager.common;

import com.axon.domain.entity.User;

import androidx.fragment.app.FragmentActivity;

/**
 * @author Alexandr Shramov
 * @date 10.12.19
 */
public interface Router {

    void transferToUsersFragment(FragmentActivity activity);

    void transferToUserFragment(FragmentActivity activity, User user);

}
