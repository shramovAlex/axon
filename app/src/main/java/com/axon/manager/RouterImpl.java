package com.axon.manager;

import com.axon.R;
import com.axon.domain.entity.User;
import com.axon.manager.common.Router;
import com.axon.ui.fragment.UserFragment;
import com.axon.ui.fragment.UsersFragment;

import androidx.annotation.IdRes;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;

/**
 * @author Alexandr Shramov
 * @date 10.12.19
 */
public class RouterImpl implements Router {

    private void replaceFragment(FragmentActivity activity,
                                 Fragment fragment,
                                 @IdRes int containerId,
                                 String tag) {
        activity.getSupportFragmentManager()
                .beginTransaction()
                .replace(containerId, fragment, tag)
                .commitAllowingStateLoss();
    }

    private void replaceFragmentAddToBAckStack(FragmentActivity activity,
                                               Fragment fragment,
                                               @IdRes int containerId,
                                               String tag) {
        activity.getSupportFragmentManager()
                .beginTransaction()
                .addToBackStack(tag)
                .replace(containerId, fragment, tag)
                .commitAllowingStateLoss();
    }

    @Override
    public void transferToUsersFragment(FragmentActivity activity) {
        replaceFragment(activity, new UsersFragment(), R.id.main_activity_container, UsersFragment.TAG);
    }

    @Override
    public void transferToUserFragment(FragmentActivity activity, User user) {
        replaceFragmentAddToBAckStack(activity, UserFragment.newInstance(user),
                                      R.id.main_activity_container, UserFragment.TAG);
    }
}
