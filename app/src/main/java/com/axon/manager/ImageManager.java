package com.axon.manager;

import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import javax.inject.Singleton;

/**
 * @author Alexandr Shramov
 * @date 10.12.19
 */
@Singleton
public class ImageManager {

    public void loadImage(ImageView imageView,
                          String url) {
        Glide.with(imageView)
                .load(url)
                .apply(RequestOptions.circleCropTransform())
                .into(imageView);
    }

}
