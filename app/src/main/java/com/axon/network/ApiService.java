package com.axon.network;

import com.axon.domain.entity.PaginatedDataWrapper;
import com.axon.domain.entity.User;

import java.util.ArrayList;

import io.reactivex.Flowable;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * @author Alexandr Shramov
 * @date 10.12.19
 */
public interface ApiService {

    @GET("api/")
    Flowable<PaginatedDataWrapper<ArrayList<User>>> getUsers(@Query("results") int count,
                                                             @Query("page") int pageToLoad,
                                                             @Query("seed") String seed);

}
