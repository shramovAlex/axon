package com.axon.network;

import com.axon.BuildConfig;

import java.util.concurrent.TimeUnit;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;

/**
 * @author Alexandr Shramov
 * @date 10.12.19
 */
public class CustomOkHttpClient extends OkHttpClient {

    private static final int CONNECT_TIMEOUT = 60;
    private static final int READ_WRITE_TIMEOUT = 120;

    public OkHttpClient getOkHttpClient() {

        final OkHttpClient.Builder okHttpClientBuilder = new OkHttpClient.Builder()
                .connectTimeout(CONNECT_TIMEOUT, TimeUnit.SECONDS)
                .writeTimeout(READ_WRITE_TIMEOUT, TimeUnit.SECONDS)
                .readTimeout(READ_WRITE_TIMEOUT, TimeUnit.SECONDS);

        if (BuildConfig.DEBUG) {
            okHttpClientBuilder.addInterceptor(setLogging());
        }

        return okHttpClientBuilder.build();
    }

    private Interceptor setLogging() {
        return new HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY);
    }

}
