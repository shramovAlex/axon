package com.axon.domain.usecase;

import com.axon.di.thread.BackgroundThread;
import com.axon.di.thread.ForegroundThread;
import com.axon.domain.entity.PaginatedDataWrapper;
import com.axon.domain.entity.User;
import com.axon.domain.params.GetUsersParam;
import com.axon.domain.repository.GetUsersRepository;
import com.axon.domain.usecase.common.BaseUseCase;

import java.util.ArrayList;

import javax.inject.Inject;

import io.reactivex.Flowable;

/**
 * @author Alexandr Shramov
 * @date 10.12.19
 */
public class GetUsersUseCase extends BaseUseCase<GetUsersParam, PaginatedDataWrapper<ArrayList<User>>> {

    private GetUsersRepository mGetUsersRepository;

    @Inject
    public GetUsersUseCase(GetUsersRepository repository,
                           ForegroundThread foregroundThread,
                           BackgroundThread backgroundThread) {
        super(foregroundThread, backgroundThread);
        mGetUsersRepository = repository;
    }

    @Override
    protected Flowable<PaginatedDataWrapper<ArrayList<User>>> buildUseCaseObservable(GetUsersParam param) {
        return mGetUsersRepository.getUsers(param);
    }
}
