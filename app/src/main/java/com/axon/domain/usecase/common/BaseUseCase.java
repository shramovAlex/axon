package com.axon.domain.usecase.common;

import com.axon.di.thread.BackgroundThread;
import com.axon.di.thread.ForegroundThread;

import io.reactivex.Flowable;
import io.reactivex.disposables.Disposable;
import io.reactivex.disposables.Disposables;
import io.reactivex.functions.Consumer;

/**
 * @author Alexandr Shramov
 * @date 10.12.19
 */
public abstract class BaseUseCase<ParamType, ResultType> {

    private ForegroundThread mForegroundThread;
    private BackgroundThread mBackgroundThread;
    protected Disposable mSubscription = Disposables.empty();

    public BaseUseCase(ForegroundThread foregroundThread,
                       BackgroundThread backgroundThread) {
        mForegroundThread = foregroundThread;
        mBackgroundThread = backgroundThread;
    }

    protected abstract Flowable<ResultType> buildUseCaseObservable(ParamType param);

    public Disposable execute(ParamType param,
                              Consumer<ResultType> onNextSubscriber,
                              Consumer<Throwable> onErrorSubscriber) {
        return mSubscription = buildUseCaseObservable(param)
                .subscribeOn(mBackgroundThread.getScheduler())
                .observeOn(mForegroundThread.getScheduler())
                .subscribe(onNextSubscriber, onErrorSubscriber);
    }

    public void unsubscribe() {
        if (mSubscription != null && !mSubscription.isDisposed()) {
            mSubscription.dispose();
        }
        mSubscription = null;
    }


}
