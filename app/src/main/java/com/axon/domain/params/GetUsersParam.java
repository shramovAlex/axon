package com.axon.domain.params;

/**
 * @author Alexandr Shramov
 * @date 10.12.19
 */
public class GetUsersParam {

    private int mPageToLoad;
    private int mCount;
    private String mSeed;

    public GetUsersParam(int pageToLoad, int count, String seed) {
        mPageToLoad = pageToLoad;
        mCount = count;
        mSeed = seed;
    }

    public int getPageToLoad() {
        return mPageToLoad;
    }

    public int getCount() {
        return mCount;
    }

    public String getSeed() {
        return mSeed;
    }
}
