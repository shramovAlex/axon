package com.axon.domain.entity;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * @author Alexandr Shramov
 * @date 10.12.19
 */
public class Street implements Parcelable {

    @SerializedName("number") private int mNumber;
    @SerializedName("name") private String name;

    public int getNumber() {
        return mNumber;
    }

    public String getName() {
        return name;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.mNumber);
        dest.writeString(this.name);
    }

    public Street() {
    }

    protected Street(Parcel in) {
        this.mNumber = in.readInt();
        this.name = in.readString();
    }

    public static final Creator<Street> CREATOR = new Creator<Street>() {
        @Override
        public Street createFromParcel(Parcel source) {
            return new Street(source);
        }

        @Override
        public Street[] newArray(int size) {
            return new Street[size];
        }
    };
}
