package com.axon.domain.entity;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * @author Alexandr Shramov
 * @date 10.12.19
 */
public class Picture implements Parcelable {

    @SerializedName("large") private String mLargeUrl;
    @SerializedName("meidum") private String mMediumUrl;
    @SerializedName("thumbnail") private String mThumbnail;

    public String getLargeUrl() {
        return mLargeUrl;
    }

    public String getMediumUrl() {
        return mMediumUrl;
    }

    public String getThumbnail() {
        return mThumbnail;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.mLargeUrl);
        dest.writeString(this.mMediumUrl);
        dest.writeString(this.mThumbnail);
    }

    public Picture() {
    }

    protected Picture(Parcel in) {
        this.mLargeUrl = in.readString();
        this.mMediumUrl = in.readString();
        this.mThumbnail = in.readString();
    }

    public static final Creator<Picture> CREATOR = new Creator<Picture>() {
        @Override
        public Picture createFromParcel(Parcel source) {
            return new Picture(source);
        }

        @Override
        public Picture[] newArray(int size) {
            return new Picture[size];
        }
    };
}
