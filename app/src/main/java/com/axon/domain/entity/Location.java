package com.axon.domain.entity;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * @author Alexandr Shramov
 * @date 10.12.19
 */
public class Location implements Parcelable {

    @SerializedName("street") Street mStreet;
    @SerializedName("city") String mCity;
    @SerializedName("state") String mState;

    public Street getStreet() {
        return mStreet;
    }

    public String getCity() {
        return mCity;
    }

    public String getState() {
        return mState;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(this.mStreet, flags);
        dest.writeString(this.mCity);
        dest.writeString(this.mState);
    }

    public Location() {
    }

    protected Location(Parcel in) {
        this.mStreet = in.readParcelable(Street.class.getClassLoader());
        this.mCity = in.readString();
        this.mState = in.readString();
    }

    public static final Creator<Location> CREATOR = new Creator<Location>() {
        @Override
        public Location createFromParcel(Parcel source) {
            return new Location(source);
        }

        @Override
        public Location[] newArray(int size) {
            return new Location[size];
        }
    };
}
