package com.axon.domain.entity;

import com.google.gson.annotations.SerializedName;

/**
 * @author Alexandr Shramov
 * @date 10.12.19
 */
public class PaginationInfo {

    @SerializedName("seed") private String mSeed;
    @SerializedName("results") private int mResults;
    @SerializedName("page") private int mPage;

    public String getSeed() {
        return mSeed;
    }

    public int getResults() {
        return mResults;
    }

    public int getPage() {
        return mPage;
    }
}
