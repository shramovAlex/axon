package com.axon.domain.entity;

import android.os.Parcel;
import android.os.Parcelable;

import com.axon.domain.entity.common.DisplayableItem;
import com.google.gson.annotations.SerializedName;

/**
 * @author Alexandr Shramov
 * @date 10.12.19
 */
public class User implements DisplayableItem, Parcelable {

    @SerializedName("name") private Name mName;
    @SerializedName("location") private Location mLocation;
    @SerializedName("email") private String mEmail;
    @SerializedName("phone") private String mPhone;
    @SerializedName("cell") private String mCell;
    @SerializedName("picture") private Picture mPicture;
    @SerializedName("dob") private DateOfBirth mDateOfBirth;
    @SerializedName("gender") private String mGender;

    public Name getName() {
        return mName;
    }

    public Location getLocation() {
        return mLocation;
    }

    public String getEmail() {
        return mEmail;
    }

    public String getPhone() {
        return mPhone;
    }

    public String getCell() {
        return mCell;
    }

    public Picture getPicture() {
        return mPicture;
    }

    public DateOfBirth getDateOfBirth() {
        return mDateOfBirth;
    }

    public String getGender() {
        return mGender;
    }

    public User() {
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(this.mName, flags);
        dest.writeParcelable(this.mLocation, flags);
        dest.writeString(this.mEmail);
        dest.writeString(this.mPhone);
        dest.writeString(this.mCell);
        dest.writeParcelable(this.mPicture, flags);
        dest.writeParcelable(this.mDateOfBirth, flags);
        dest.writeString(this.mGender);
    }

    protected User(Parcel in) {
        this.mName = in.readParcelable(Name.class.getClassLoader());
        this.mLocation = in.readParcelable(Location.class.getClassLoader());
        this.mEmail = in.readString();
        this.mPhone = in.readString();
        this.mCell = in.readString();
        this.mPicture = in.readParcelable(Picture.class.getClassLoader());
        this.mDateOfBirth = in.readParcelable(DateOfBirth.class.getClassLoader());
        this.mGender = in.readString();
    }

    public static final Creator<User> CREATOR = new Creator<User>() {
        @Override
        public User createFromParcel(Parcel source) {
            return new User(source);
        }

        @Override
        public User[] newArray(int size) {
            return new User[size];
        }
    };
}
