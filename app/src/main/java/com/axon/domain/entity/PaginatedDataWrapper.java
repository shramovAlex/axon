package com.axon.domain.entity;

import com.google.gson.annotations.SerializedName;

/**
 * @author Alexandr Shramov
 * @date 10.12.19
 */
public class PaginatedDataWrapper<DataType> {

    @SerializedName("results") private DataType mData;
    @SerializedName("info") private PaginationInfo mPaginationInfo;

    public DataType getData() {
        return mData;
    }

    public PaginationInfo getPaginationInfo() {
        return mPaginationInfo;
    }
}
