package com.axon.domain.entity;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * @author Alexandr Shramov
 * @date 11.12.19
 */
public class DateOfBirth implements Parcelable {

    @SerializedName("date") private String mDate;
    @SerializedName("age") private int mAge;

    public String getDate() {
        return mDate;
    }

    public int getAge() {
        return mAge;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.mDate);
        dest.writeInt(this.mAge);
    }

    public DateOfBirth() {
    }

    protected DateOfBirth(Parcel in) {
        this.mDate = in.readString();
        this.mAge = in.readInt();
    }

    public static final Creator<DateOfBirth> CREATOR = new Creator<DateOfBirth>() {
        @Override
        public DateOfBirth createFromParcel(Parcel source) {
            return new DateOfBirth(source);
        }

        @Override
        public DateOfBirth[] newArray(int size) {
            return new DateOfBirth[size];
        }
    };
}
