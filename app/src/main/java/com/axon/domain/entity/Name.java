package com.axon.domain.entity;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * @author Alexandr Shramov
 * @date 10.12.19
 */
public class Name implements Parcelable {

    @SerializedName("title") private String mTitle;
    @SerializedName("last") private String mLast;
    @SerializedName("first") private String mFirst;

    public String getTitle() {
        return mTitle;
    }

    public String getLast() {
        return mLast;
    }

    public String getFirst() {
        return mFirst;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.mTitle);
        dest.writeString(this.mLast);
        dest.writeString(this.mFirst);
    }

    public Name() {
    }

    protected Name(Parcel in) {
        this.mTitle = in.readString();
        this.mLast = in.readString();
        this.mFirst = in.readString();
    }

    public static final Creator<Name> CREATOR = new Creator<Name>() {
        @Override
        public Name createFromParcel(Parcel source) {
            return new Name(source);
        }

        @Override
        public Name[] newArray(int size) {
            return new Name[size];
        }
    };
}
