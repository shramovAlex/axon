package com.axon.domain.repository;

import com.axon.domain.entity.PaginatedDataWrapper;
import com.axon.domain.entity.User;
import com.axon.domain.params.GetUsersParam;

import java.util.ArrayList;

import io.reactivex.Flowable;

/**
 * @author Alexandr Shramov
 * @date 10.12.19
 */
public interface GetUsersRepository {

    Flowable<PaginatedDataWrapper<ArrayList<User>>> getUsers(GetUsersParam param);

    Flowable<PaginatedDataWrapper<ArrayList<User>>> getRemoteUsers(GetUsersParam param);

}
