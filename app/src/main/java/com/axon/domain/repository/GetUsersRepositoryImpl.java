package com.axon.domain.repository;

import com.axon.domain.entity.PaginatedDataWrapper;
import com.axon.domain.entity.User;
import com.axon.domain.params.GetUsersParam;
import com.axon.network.ApiService;

import java.util.ArrayList;

import javax.inject.Inject;

import io.reactivex.Flowable;

/**
 * @author Alexandr Shramov
 * @date 10.12.19
 */
public class GetUsersRepositoryImpl implements GetUsersRepository {

    private final ApiService mApiService;

    @Inject
    public GetUsersRepositoryImpl(ApiService apiService) {
        mApiService = apiService;
    }

    @Override
    public Flowable<PaginatedDataWrapper<ArrayList<User>>> getUsers(GetUsersParam param) {
        return getRemoteUsers(param);
    }

    @Override
    public Flowable<PaginatedDataWrapper<ArrayList<User>>> getRemoteUsers(GetUsersParam param) {
        return mApiService.getUsers(param.getCount(),
                                    param.getPageToLoad(),
                                    param.getSeed());
    }
}
