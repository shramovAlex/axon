package com.axon.di.component;

import com.axon.di.module.AppModule;
import com.axon.di.module.RepositoryModule;
import com.axon.di.module.SchedulersModule;
import com.axon.mvp.presenter.UserFragmentPresenter;
import com.axon.mvp.presenter.UsersFragmentPresenter;
import com.axon.ui.activity.MainActivity;
import com.axon.ui.fragment.UserFragment;
import com.axon.ui.fragment.UsersFragment;

import dagger.Component;

/**
 * @author Alexandr Shramov
 * @date 10.12.19
 */
@Component(modules = {AppModule.class, RepositoryModule.class, SchedulersModule.class})
public interface AppComponent {

    void inject(MainActivity activity);

    void inject(UsersFragmentPresenter presenter);

    void inject(UsersFragment fragment);

    void inject(UserFragmentPresenter presenter);

    void inject(UserFragment fragment);

}
