package com.axon.di.thread;

import javax.inject.Singleton;

import io.reactivex.Scheduler;
import io.reactivex.schedulers.Schedulers;

/**
 * @author Alexandr Shramov
 * @date 10.12.19
 */
@Singleton
public class DefaultBackgroundThread implements BackgroundThread {

    @Override
    public Scheduler getScheduler() {
        return Schedulers.io();
    }
}
