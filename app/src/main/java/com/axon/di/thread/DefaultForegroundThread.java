package com.axon.di.thread;

import javax.inject.Singleton;

import io.reactivex.Scheduler;
import io.reactivex.android.schedulers.AndroidSchedulers;

/**
 * @author Alexandr Shramov
 * @date 10.12.19
 */
@Singleton
public class DefaultForegroundThread implements ForegroundThread {

    @Override
    public Scheduler getScheduler() {
        return AndroidSchedulers.mainThread();
    }

}
