package com.axon.di.thread;

import javax.inject.Singleton;

import io.reactivex.Scheduler;

/**
 * @author Alexandr Shramov
 * @date 10.12.19
 */
@Singleton
public interface ForegroundThread {

    Scheduler getScheduler();

}
