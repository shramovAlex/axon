package com.axon.di.module;

import android.app.Application;
import android.content.Context;

import com.axon.manager.ImageManager;
import com.axon.manager.RouterImpl;
import com.axon.manager.common.Router;
import com.axon.network.ApiService;
import com.axon.network.CustomOkHttpClient;
import com.axon.utils.Constants;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;

/**
 * @author Alexandr Shramov
 * @date 10.12.19
 */
@Module
public class AppModule {

    private final Context mContext;
    private Gson mGson;
    private ApiService mApi;
    private ImageManager mImageManager;
    private Router mRouter;

    public AppModule(Application application) {
        mContext = application.getApplicationContext();
    }

    @Provides
    @Singleton
    Context provideContext() {
        return mContext;
    }

    @Provides
    CustomOkHttpClient provideCustomOkHttpClient() {
        return new CustomOkHttpClient();
    }

    @Provides
    @Singleton
    Gson provideGson() {
        if (mGson == null) {
            return mGson = new GsonBuilder()
                    .create();
        }
        return mGson;
    }

    @Provides
    ApiService provideApi() {
        if (mApi == null) {
            return mApi = new Retrofit.Builder()
                    .baseUrl(Constants.BASE_URL)
                    .addConverterFactory(ScalarsConverterFactory.create())
                    .addConverterFactory(GsonConverterFactory.create(provideGson()))
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .client(provideCustomOkHttpClient().getOkHttpClient())
                    .build()
                    .create(ApiService.class);
        }
        return mApi;
    }

    @Provides
    ImageManager provideImageManager() {
        if(mImageManager == null) {
            return new ImageManager();
        }
        return mImageManager;
    }

    @Provides
    Router provideRouter() {
        if(mRouter == null) {
            mRouter = new RouterImpl();
        }
        return mRouter;
    }

}
