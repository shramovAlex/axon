package com.axon.di.module;

import com.axon.di.thread.BackgroundThread;
import com.axon.di.thread.DefaultBackgroundThread;
import com.axon.di.thread.DefaultForegroundThread;
import com.axon.di.thread.ForegroundThread;

import dagger.Module;
import dagger.Provides;

/**
 * @author Alexandr Shramov
 * @date 10.12.19
 */
@Module
public class SchedulersModule {

    @Provides
    DefaultForegroundThread provideDefaultForegroundThread() {
        return new DefaultForegroundThread();
    }

    @Provides
    ForegroundThread provideForegroundThread(DefaultForegroundThread thread) {
        return thread;
    }

    @Provides
    DefaultBackgroundThread provideDefaultBackgroundThread() {
        return new DefaultBackgroundThread();
    }

    @Provides
    BackgroundThread provideBackgroundThread(DefaultBackgroundThread thread) {
        return thread;
    }

}
