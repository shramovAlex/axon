package com.axon.di.module;

import com.axon.domain.repository.GetUsersRepository;
import com.axon.domain.repository.GetUsersRepositoryImpl;

import dagger.Binds;
import dagger.Module;

/**
 * @author Alexandr Shramov
 * @date 10.12.19
 */
@Module
public abstract class RepositoryModule {

    @Binds
    public abstract GetUsersRepository bindUsersRepository(GetUsersRepositoryImpl repository);

}
